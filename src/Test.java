// import not working

public class Test {
    public static void main(String[] args) {
        System.out.println(" doubly Circular linked list program \n");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Perform actions: 1. Add First \n" +
                "\n" +
                "2. Add Last \n" +
                "\n" +
                "3. Add at Specific Position\n" +
                "\n" +
                "4.Delete Last\n" +
                "\n" +
                "5.Display List");

        int choice = scanner.nextInt();
        switch (choice) {
            // switch between chocies and call appropriate methods...
        }
    }
}

class LinkedList {
    private backingList = new ArrayList<Element>();

    //1. ADD FIRST
    public add(Book b) {
        // To add at the end
        if (backingList.size() == 0) {
            // list empty, simply add
            Element element = new Element();
            element.current = book;
            element.next = null;
            element.prev = null;
            backingList.add(b);
        } else {
            // get first book, and last book to make list circular
            Element first = backingList.get(0);
            Element last = backingList.get(backingList.size() - 1);
            Element element = new Element();
            element.prev = last;
            element.next = first;
            // also alter first node reference to reflect last addition
            first.prev = element;
        }

    }


    // 2. ADD LAST
    public add(Book b) {
        // To add at the end
        if (backingList.size() == 0) {
            // list empty, simply add
            Element element = new Element();
            element.current = book;
            element.next = null;
            element.prev = null;
            backingList.add(b);
        } else {
            // get last book, and first book to make list circular
            Element first = backingList.get(0);
            Element last = backingList.get(backingList.size() - 1);
            Element element = new Element();
            element.prev = last;
            element.next = first;
            backingList.add(b);
            // also alter first node reference to reflect last addition
            first.prev = element;
        }

    }

    //3. ADD AT SPECIFIC POSITION
    public add(Book book, int index) {
        // To add at specific position
        if (backingList.size() == 0 && index == 0) {
            // list empty, simply add
            Element element = new Element();
            element.current = book;
            element.next = null;
            element.prev = null;
            backingList.add(b);
        } else if (index > 0) {
            // get book at that index , and modify its pointers to refer its neighbours
            Element presetNode = backingList.get(index); // to be forward neighbour
            Element backwordNeighbour = backingList.get(index - 1);


            // alter its pointrs to point to left and right element
            Element inbound = new Element();
            inbound.current = book;
            inbound.next = presetNode;
            inbound.prev = backwordNeighbour;

            // alter neigbour's pointers to refer to this newly added element
            backwordNeighbour.next = inbound;
            presetNode.prev = inbound;
        }
    }

    //4. DELETE LAST
    public remove() {
        // remove from end
        int listSize = backingList.size()
        // only if list not empty
        if (listSize > 0) {

            // Two cases, if list size is 1 can't have a circular list, or if more than 1
            if (listSize == 1) {
                backingList.remove(); // only remove
            } else {
                backingList.remove();
                // change the lists last element's ponters to point to the first node again
                Element newLastElement = backingList.get(backingList.size() - 1); // after removal
                Element firstElement =  backingList.get(0);
                newLastElement.next = firstElement;
                firstElement.prev = newLastElement;
            }
        }
    }

    //5. DISPLAY LIST
    public printAll() {
        for (Element el: backingList) {
            System.out.println(el)
        }
    }

}

class Element {
    Book current;
    Book next;
    Book prev;

    @java.lang.Override
    public java.lang.String toString() {
        return current.toString() + "\nNext -" + next.getName() + "\nPrev - " + prev.getName()
    }
}

class Book {
    String name;
    String author;
    int numberOfPages;
    Double price;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(int numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @java.lang.Override
    public java.lang.String toString() {
        return "Book{" +
                "name='" + name + '\'' +
                ", author='" + author + '\'' +
                ", numberOfPages=" + numberOfPages +
                ", price=" + price +
                '}';
    }

}

